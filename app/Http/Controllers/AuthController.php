<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
    	return view('page.register');
    }

    public function welcome(Request $request){
    	$fnama = $request->fnama;
    	$lnama = $request->lnama;
    	return view('page.welcome', compact('fnama','lnama'));
    }
}
