@extends('adminlte.master')

@section ('content')

<div class="card">
	<div class="card-header">
		<h2>Show Pertanyaan {{$pertanyaan->id}}</h2>
	</div>
	<div class="card-body">
		<h4>{{$pertanyaan->judul}}</h4>
		<p>{{$pertanyaan->isi}}</p>
	</div>
</div>

@endsection