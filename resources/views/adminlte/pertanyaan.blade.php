@extends('adminlte.master')

@section ('content')

<div class="card">
	<div class="card-header">
    	<h3 class="card-title">Daftar Pertanyaan</h3>
    </div>
    <div class="card-body">
		<a href="/pertanyaan/create" class="btn btn-primary mb-3">Tambah</a>
        <table class="table table-bordered" id="contoh">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Judul</th>
                <th scope="col">Isi</th>
                <th scope="col" style="display: block;">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($pertanyaan as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->judul}}</td>
                        <td>{{$value->isi}}</td>
                        <td>
                            <a href="/pertanyaan/{{$value->id}}" class="btn btn-info my-1 ml-1 d">Show</a>
                            <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-primary my-1 ml-1">Edit</a>
                            <form action="/pertanyaan/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger ml-1 my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
    </div>

</div>

@endsection

@push('scripts')
    
    <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
      $(function () {
        $("contoh").DataTable();
      });
    </script>

@endpush