<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width-device-width, initial-scale=1.0">
	<title>Form</title>
</head>
<body>
<h1>Buat Account Baru!</h1>
<h2>Sign Up Form</h2>
<form action="/welcome" method="POST">
	@csrf
	<label for="fnama">First Name : </label><br>
	<input id="fnama" type="text" name="fnama"><br><br>
	<label for="lnama">Last Name : </label><br>
	<input id="lnama" type="text" name="lnama"><br><br>
	<label for="gender">Gender :</label><br>
	<input type="radio" name="gender">Male <br>
	<input type="radio" name="gender">Female <br>
	<input type="radio" name="gender">Other <br><br>
	<label for="nationality">Nationality :</label><br>
	<select name="nationality">
		<option value="1"> Indonesians </option>
		<option value="1"> Americans </option>
		<option value="1"> Chinese </option>
		<option value="1"> British </option>
	</select><br><br>
	<label for="language">Language Spoken :</label><br>
	<input type="checkbox" name=""language> Bahasa Indonesia <br>
	<input type="checkbox" name=""language> English <br>
	<input type="checkbox" name=""language> Other <br><br>	
	<label for="bio">Bio :</label><br>
	<textarea name="bio" cols="30" rows="10"></textarea><br><br>
	<input type="submit" value="Submit">
	<input type="reset" value="Clear">
</form>
</body>
</html>